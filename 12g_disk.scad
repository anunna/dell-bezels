module dell_12g_disk_blank() {
  difference() {
    union() {
      translate([-104/2,0,0])linear_extrude (26.25) polygon([[0,0],
        [0,22.76],
        [10,24.09],
        [30,26.95],
        [40,27.26],
        [50,28.52],
        [60,29.13],
        [70,28.33],
        [80,27.59],
        [90,25.6],
        [100,23.2],
        [104,22.5],
        [104,0]]);
      
      intersection() {
        translate([-104/2,0,0])linear_extrude (26.25) polygon([[0,0],
          [10,0],
          [10,29.4],
          [20,27.78],
          [30,26.95],
          [30,0]]);
        translate([-104/2+19/2+14.6,16,26.25/2])rotate([90,0,0])cylinder(r=(19+9)/2,h=30,center=true);
      }
    }
    translate([0,20/2,26.25/2])cube([102,19,24.25],center=true);
    hull() {
      translate([-104/2+19/2+16.6,16,26.25/2])rotate([90,0,0])cylinder(r=19/2,h=30,center=true);
      translate([-104/2+32,16,26.25/2])rotate([90,0,0])cylinder(r=18/2,h=30,center=true);
    } hull() {
      translate([-104/2+32,16,26.25/2])rotate([90,0,0])cylinder(r=18/2,h=30,center=true);
      translate([-104/2+34,16,26.25/2])rotate([90,0,0])cylinder(r=16/2,h=30,center=true);
    } hull() {
      translate([-104/2+34,16,26.25/2])rotate([90,0,0])cylinder(r=16/2,h=30,center=true);
      translate([-104/2+38.6,16,26.25/2])rotate([90,0,0])cylinder(r=12/2,h=30,center=true);
    } hull() {
      translate([-104/2+38.6,16,26.25/2])rotate([90,0,0])cylinder(r=12/2,h=30,center=true);
      translate([-104/2+46.2,16,26.25/2])rotate([90,0,0])cylinder(r=10/2,h=30,center=true);
    } hull() {
      translate([-104/2+46.2,16,26.25/2])rotate([90,0,0])cylinder(r=10/2,h=30,center=true);
      translate([-104/2+51.5,16,26.25/2])rotate([90,0,0])cylinder(r=9/2,h=30,center=true);
    } hull() {
      translate([-104/2+51.5,16,26.25/2])rotate([90,0,0])cylinder(r=9/2,h=30,center=true);
      translate([-104/2+56,16,26.25/2])rotate([90,0,0])cylinder(r=8/2,h=30,center=true);
    } hull() {
      translate([-104/2+56,16,26.25/2])rotate([90,0,0])cylinder(r=8/2,h=30,center=true);
      translate([-104/2+62,16,26.25/2])rotate([90,0,0])cylinder(r=7/2,h=30,center=true);
    } hull() {
      translate([-104/2+62,16,26.25/2])rotate([90,0,0])cylinder(r=7/2,h=30,center=true);
      translate([104/2-35,16,26.25/2])rotate([90,0,0])cylinder(r=6.75/2,h=30,center=true);
    } hull() {
      translate([104/2-35,16,26.25/2])rotate([90,0,0])cylinder(r=6.75/2,h=30,center=true);
      translate([104/2-28,16,26.25/2])rotate([90,0,0])cylinder(r=7.5/2,h=30,center=true);
    } hull() {
      translate([104/2-28,16,26.25/2])rotate([90,0,0])cylinder(r=7.5/2,h=30,center=true);
      translate([104/2-26,16,26.25/2])rotate([90,0,0])cylinder(r=8/2,h=30,center=true);
    } hull() {
      translate([104/2-26,16,26.25/2])rotate([90,0,0])cylinder(r=8/2,h=30,center=true);
      translate([104/2-22,16,26.25/2])rotate([90,0,0])cylinder(r=8.5/2,h=30,center=true);
    } hull() {
      translate([104/2-22,16,26.25/2])rotate([90,0,0])cylinder(r=8.5/2,h=30,center=true);
      translate([104/2-15.6,16,26.25/2])rotate([90,0,0])cylinder(r=10/2,h=30,center=true);
    } hull() {
      translate([104/2-15.6,16,26.25/2])rotate([90,0,0])cylinder(r=10/2,h=30,center=true);
      translate([104/2-10.4/2-9.7,16,26.25/2])rotate([90,0,0])cylinder(r=10.4/2,h=30,center=true);
    }
//    }
  }
}
module label(name="TEST",depth=5) {
  scale(1.6)
  difference() {
    translate([0,0,5.5-depth])
   minkowski() {
     linear_extrude(depth)
     text(name,
         font="Liberation Sans",
         halign="center",
         valign="center");
     cube([3.5,3.5,1],center=true);
   }
   translate([0,0,2.5])
    linear_extrude(4)
     text(name,
        font="Liberation Sans",
        halign="center",
        valign="center");
 }
}


module nameplate(name) {
 translate([104/2-10.4/2-9.7,14,26.25/2]) {
    rotate([90,0,0]) intersection() {
      union() {
        translate([0,0,9])cylinder(r=18/2,h=16,center=true);
        translate([0,0,-1])cylinder(r1=10/2,r2=18/2,h=8/2,center=true);
      }
    cube([10,18,20],center=true);
   }
      translate([0,10,0])rotate([90,0,0]) cylinder(r=10.4/2,h=15,center=true);
 }
      translate([-104/2+19/2+16.6,25,26.25/2])rotate([90,0,0])cylinder(r1=18.8/2,r2=19.1/2,h=12,center=true);
  translate([0,30,13]) rotate([90,0,180])label(name);
}

translate([0,0,39.5])rotate([-90,0,0]) {
//difference() {
//  dell_12g_disk_blank();
//  translate([0,0,115])cube([200,200,200],center=true);
//}
nameplate("NODE001");
}
