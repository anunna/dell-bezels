BEZEL_LENGTH=431;
BEZELEND_LENGTH=26.5;
BEZEL_WIDTH=37;
DIMPLE_HEIGHT=8;

module hexhole() {
  //This should be a generic hole in all 14G bezels.
    HOLE_WIDTH=26;
    HOLE_LENGTH=40;
    HEX_CORNER=HOLE_LENGTH/2;
  rotate(90)
    linear_extrude(height=40,scale=1.25)
        polygon(points=[[-HOLE_LENGTH/2,0],
                        [-HEX_CORNER/2,HOLE_WIDTH/2],
                        [HEX_CORNER/2,HOLE_WIDTH/2],
                        [HOLE_LENGTH/2,0],
                        [HEX_CORNER/2,-HOLE_WIDTH/2],
                        [-HEX_CORNER/2,-HOLE_WIDTH/2]]);
}

module bezelend(u=1) {
  //The bezels have angled ends for clamps, connectors etc.
    rotate([90,0,-90])
    translate([0,0,-u*BEZEL_WIDTH/2+4])
    linear_extrude((u*BEZEL_WIDTH)-8)
    polygon(points=[[0,0],
                 [BEZELEND_LENGTH,0],
                 [BEZELEND_LENGTH,15.5],
                 [0,6.5]]);
}

module lock() {
  //There's a little cylindical lock on the top corner of each bezel.
  union() {
  translate([0,0,9/2+8]) cylinder(r1=18/2,r2=16/2,h=9,center=true);
  translate([0,0,9+8-1]) cylinder(r=15/2,h=5.5,center=true);
  }
}
  
module edge () {
    //Rails that run the full length of the bezel end-to-end
    rotate([90,0,0])
    translate([0,0,-BEZEL_LENGTH/2+5])
    linear_extrude(BEZEL_LENGTH-10)
    polygon(points=[[-3,0],[-2.25,8],[2.25,8],[3,0]]);
}
module bezel_back_dimple(u=1) {
    //Cutout on the back of the bezels
translate([0,(BEZEL_LENGTH-2*BEZELEND_LENGTH)/2,0])
  intersection() {
    rotate([90,0,0])
    linear_extrude(BEZEL_LENGTH-2*BEZELEND_LENGTH)
      polygon(points=[
        [-u*BEZEL_WIDTH/2+12, -0.01],
        [-u*BEZEL_WIDTH/2+18, DIMPLE_HEIGHT],
        [u*BEZEL_WIDTH/2-18, DIMPLE_HEIGHT],
        [u*BEZEL_WIDTH/2-12, -0.01]]);

    translate([u*BEZEL_WIDTH/2,0,0])
    rotate([90,0,-90])
    linear_extrude((u*BEZEL_WIDTH))
      polygon(points=[
        [0, -0.01],
        [6, 6],
        [BEZEL_LENGTH-2*BEZELEND_LENGTH-6, DIMPLE_HEIGHT],
        [BEZEL_LENGTH-2*BEZELEND_LENGTH, -0.01]]);
  }
}

module bezel_grid (u=1) {
  difference() {
  translate([0,(BEZEL_LENGTH-2*BEZELEND_LENGTH)/2,0])
    intersection() {
      rotate([90,0,0])
        linear_extrude(BEZEL_LENGTH)
          polygon(points=[
            [(-u*BEZEL_WIDTH/2)+4, 0],
            [(-u*BEZEL_WIDTH/2)+4, 8.5],
            [(-u*BEZEL_WIDTH/2)+18, 20],
            [(u*BEZEL_WIDTH/2)-18, 20],
            [(u*BEZEL_WIDTH/2)-4, 8.5],
            [(u*BEZEL_WIDTH/2)-4, 0]]);

      rotate([90,0,-90])
        translate([0,0,-(u*BEZEL_WIDTH/2)+2])
          linear_extrude((u*BEZEL_WIDTH)-4)
            polygon(points=[
              [0, 0],
              [0, 8.5],
              [18, 20],
              [BEZEL_LENGTH-2*BEZELEND_LENGTH-18, 20],
              [BEZEL_LENGTH-2*BEZELEND_LENGTH, 8.5],
              [BEZEL_LENGTH-2*BEZELEND_LENGTH, 0]]);
    }
    //2U+1 rows
    translate([0,((u-1)%2)*BEZEL_LENGTH*0.0875,0]) //Empirical value here
    for (i = [-u:1:u]) {
      translate([(i*BEZEL_WIDTH/2),(abs(i)%2)*(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.1,0]) {
        translate([0,(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.5,0]) hexhole();
        translate([0,(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.3,0]) hexhole();
        translate([0,(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.1,0]) hexhole();
        translate([0,-(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.1,0]) hexhole();
        translate([0,-(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.3,0]) hexhole();
        translate([0,-(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.5,0]) hexhole();
        translate([0,-(BEZEL_LENGTH-2*BEZELEND_LENGTH)*0.7,0]) hexhole();
      }
    }
    bezel_back_dimple(u);
  }
    
}

module bezel(u=1) {
    //Bring it all together
    rotate(90)
    union() {
        bezel_grid(u);
        translate([0,BEZEL_LENGTH/2,0]) bezelend(u);
        translate([0,-BEZEL_LENGTH/2,0]) rotate(180) bezelend(u);
        translate([u*BEZEL_WIDTH/2-4-3,0,0]) edge();
        translate([-u*BEZEL_WIDTH/2+4+3,0,0]) edge();
        translate([(u-1)*BEZEL_WIDTH/2,(BEZEL_LENGTH/2-BEZELEND_LENGTH)*0.871,1.5]) lock();
    }
}

module label(name="TEST",depth=5) {
  scale(1.8)
  difference() {
    translate([0,0,5.5-depth])
   minkowski() {
     linear_extrude(depth)
     text(name,
         font="Liberation Sans",
         halign="center",
         valign="center");
     cube([3.5,3.5,1],center=true);
   }
   translate([0,0,2.5])
    linear_extrude(4)
     text(name,
        font="Liberation Sans",
        halign="center",
        valign="center");
 }
}


//difference() {
//  %translate([20,10,18])cube([10,10,4.5],center=true);
//  translate([0,0,16]) label("FAT100");
//  translate([-22,10,12.5]) cube([3,10,15],center=true);
//  translate([-22,-10,12.5]) cube([3,10,15],center=true);
//  translate([22,10,12.5]) cube([3,10,15],center=true);
//  translate([22,-10,12.5]) cube([3,10,15],center=true);
//  translate([47,0,12.5]) cube([50,40,15],center=true);
//  translate([-47,0,12.5]) cube([50,40,15],center=true);

    //Width of a 2-bezel hex hole
//translate([0,0,14.5]) %cube([20,23,13],center=true);
//  bezel(2);
//}

difference() {
  translate([0,0,10]) label("ROUTER0");
  bezel(1);

    //Length of a 1-bezel bar
 //%translate([0,0,13.5]) %cube([32.6,8.5,13],center=true);
}
