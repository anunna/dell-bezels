GRID_PITCH=5;
GRID_HOLE=3.25;
GRID_HOLE_DEPTH=5;
GRID_TAPER=1.25;

module label(name="TEST",depth=10) {
  scale([1.8,1.8,1])
  difference() {
    translate([0,0,0.5])
   minkowski() {
     linear_extrude(depth-1)
     text(name,
         font="Liberation Sans",
         halign="center",
         valign="center");
     cube([3.5,3.5,1],center=true);
   }
   translate([0,0,depth/2])
    linear_extrude(depth)
     text(name,
        font="Liberation Sans",
        halign="center",
        valign="center");
 }
}

module grid_bump() {
 rotate([0,90,0])
  intersection() {
  translate([0,0,-GRID_HOLE*GRID_TAPER/2])
    linear_extrude(height=GRID_HOLE*GRID_TAPER)
      polygon([[0,-GRID_HOLE/2*GRID_TAPER],
           [GRID_HOLE_DEPTH,-GRID_HOLE/2/GRID_TAPER],
           [GRID_HOLE_DEPTH,GRID_HOLE/2/GRID_TAPER],
           [0,GRID_HOLE/2*GRID_TAPER]]);
  rotate([90,0,0])
  translate([0,0,-GRID_HOLE*GRID_TAPER/2])
    linear_extrude(height=GRID_HOLE*GRID_TAPER)
      polygon([[0,-GRID_HOLE/2*GRID_TAPER],
           [GRID_HOLE_DEPTH,-GRID_HOLE/2/GRID_TAPER],
           [GRID_HOLE_DEPTH,GRID_HOLE/2/GRID_TAPER],
           [0,GRID_HOLE/2*GRID_TAPER]]);
  }
}

module nameplate(name) {
  label(name,9-GRID_HOLE_DEPTH/2);
  for (pos=[[6,1.75],[6,-1.75],[-6,1.75],[-6,-1.75]]) {
    translate([pos[0]*GRID_PITCH,pos[1]*GRID_PITCH,0]) grid_bump();
  }
}
  


rotate([180,0,0]) {
nameplate("GPU100");
}
